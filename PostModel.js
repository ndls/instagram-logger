const mongoose = require('mongoose')

const Post = mongoose.model('Post', {
  hashtag: { type: String, required: true },
  shortcode: { type: String, required: true }
})

module.exports = Post
