(async () => {
  if (!process.argv[2]) {
    console.log(`no hashtag, exiting...`)
    process.exit()
  }

  const mongoose = require('mongoose')
  const axios = require('axios')

  const hashtag = process.argv[2]
  const interval = 15000
  const url = `https://www.instagram.com/explore/tags/${hashtag}/recent/?__a=1`
  const db = 'mongodb://localhost:27017/instagram'

  const handler = {}

  function errorHandler (error) {
    switch (error.type) {
      case 'web':
        console.log(`${error.type} error: ${error.message}`)
        console.log(`trying again in ${interval / 1000} seconds...`)
        setTimeout(() => {
          fetcher()
        }, interval)
        break
      case 'mongo':
        console.log(`${error.type} error: ${error.message}`)
        console.log(`exiting...`)
        process.exit()
      case 'formatting':
        console.log(`${error.type} error: ${error.message}`)
        console.log(`exiting...`)
        process.exit()
      default:
        console.log(`${error.type} error: ${error.message}`)
        console.log(`exiting...`)
        process.exit()
    }
  }

  try {
    await mongoose.connect(db, { useNewUrlParser: true })
    console.log(`connected to db`)
  } catch (error) {
    error.type = 'mongo'
    return errorHandler(error)
  }

  const Post = require('./PostModel')

  async function fetcher () {
    const t = new Date()
    console.log(`\n`)
    console.log(new Date().toLocaleTimeString())
    console.log(`fetching: #${hashtag}`)

    try {
      handler.response = await axios.get(url)
    } catch (error) {
      error.type = 'web'
      return errorHandler(error)
    }

    try {
      handler.obj = handler.response.data.graphql.hashtag.edge_hashtag_to_media.edges
      handler.hashtag = handler.response.data.graphql.hashtag.name
    } catch (error) {
      error.type = 'formatting'
      return errorHandler(error)
    }

    try {
      handler.fetched = 0
      handler.saved = 0
      handler.skipped = 0
      for (let prop in handler.obj) {
        handler.fetched++
        handler.foundPost = await Post.find({ shortcode: handler.obj[prop].node.shortcode })
        if (handler.foundPost.length > 0) {
          handler.skipped++
        } else {
          const post = new Post({
            hashtag: handler.hashtag,
            shortcode: handler.obj[prop].node.shortcode
          })
          await post.save()
          handler.saved++
        }
      }

      console.log(`saved: ${handler.saved}`)
      console.log(`skipped: ${handler.skipped}`)
      console.log(`fetched: ${handler.fetched}`)
    } catch (error) {
      error.type = 'mongo'
      return errorHandler(error)
    }

    try {
      handler.total = await Post.find({ hashtag: hashtag })
      console.log(`total pool size: ${handler.total.length}`)
    } catch (error) {
      error.type = 'mongo'
      return errorHandler(error)
    }

    console.log(`it took: ${new Date() - t}ms`)
    console.log(`trying again in ${interval / 1000} seconds...`)

    setTimeout(() => {
      fetcher()
    }, interval)
  }

  await fetcher()
})()
